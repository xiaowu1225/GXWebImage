# GXWebImage

[![CI Status](https://img.shields.io/travis/xiaowu1225/GXWebImage.svg?style=flat)](https://travis-ci.org/xiaowu1225/GXWebImage)
[![Version](https://img.shields.io/cocoapods/v/GXWebImage.svg?style=flat)](https://cocoapods.org/pods/GXWebImage)
[![License](https://img.shields.io/cocoapods/l/GXWebImage.svg?style=flat)](https://cocoapods.org/pods/GXWebImage)
[![Platform](https://img.shields.io/cocoapods/p/GXWebImage.svg?style=flat)](https://cocoapods.org/pods/GXWebImage)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

GXWebImage is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'GXWebImage'
```

## Author

xiaowu1225, suiyuan78223@163.com

## License

GXWebImage is available under the MIT license. See the LICENSE file for more info.
