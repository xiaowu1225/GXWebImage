//
//  GXAppDelegate.h
//  GXWebImage
//
//  Created by xiaowu1225 on 05/06/2019.
//  Copyright (c) 2019 xiaowu1225. All rights reserved.
//

@import UIKit;

@interface GXAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
