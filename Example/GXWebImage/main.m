//
//  main.m
//  GXWebImage
//
//  Created by xiaowu1225 on 05/06/2019.
//  Copyright (c) 2019 xiaowu1225. All rights reserved.
//

@import UIKit;
#import "GXAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([GXAppDelegate class]));
    }
}
